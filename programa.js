class mapa{
    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];

    constructor(){
        this.posicionInicial=[4.763933,-74.08859];
        this.escalaInicial=17;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:20,
        };

        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial, this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL, this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }

    colocarMarcador(posicion){
        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);

    }    

    colocarCirculo(posicion, configuracion){
        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor); 
        
    }

    colocarPoligono(posicion, configuracion){
        this.poligonos.push(L.polygon(posicion, configuracion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);
    }
}

let miMapa=new mapa();
miMapa.colocarMarcador([4.763933,-74.080859]);
miMapa.colocarMarcador([4.763891,-74.082573]);
miMapa.colocarMarcador([4.763377,-74.081993]);

miMapa.colocarCirculo([4.763933,-74.080859],{
    color:'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 50
});
miMapa.colocarCirculo([4.765666,-74.071650],{
    color:'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 50
});
miMapa.colocarCirculo([4.766179,-74.079170],{
    color:'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 50
});

miMapa.colocarPoligono([[4.762746,-74.081393], [4.761891,-74.081199], [4.762201,-74.080802]],{
    fillColor: "blue",
    opacity: 0.5,
    color:"blue"
});

miMapa.colocarPoligono([[4.761998,-74.084420], [4.763698,-74.084356], [4.763409,-74.082874]],{
    fillColor: "blue",
    opacity: 0.5,
    color:"blue"
});

miMapa.colocarPoligono([[4.764843,-74.087952], [4.766832,-74.085880], [4.764404,-74.096619]],{
    fillColor: "blue",
    opacity: 0.5,
    color:"blue"
});
